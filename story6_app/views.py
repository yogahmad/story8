from django.shortcuts import render, redirect
from .models import pesan_model
from .forms import pesan_form
from django.http import HttpResponse

# Create your views here.
def index(request):
    data = pesan_model.objects.all().order_by('-tanggal')
    context = {
        'list_pesan' : data,
    }
    return render(request, 'story6_app/index.html', context)


def tambah(request):
    if(request.method == 'POST'):
        form = pesan_form(request.POST)
        if(form.is_valid()):
            pesan = request.POST['pesan']
            data = pesan_model(pesan = pesan)
            data.save()
    return redirect('/')

def hapus(request):
    if(request.method == 'POST'):
        id = request.POST['id']
        data = pesan_model.objects.get(id = int(id))
        data.delete()
    return redirect('/')

def profile(request):
    return render(request, 'story6_app/profile.html')

def search(request):
    return render(request, 'story6_app/search.html')