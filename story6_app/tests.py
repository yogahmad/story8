from django.test import TestCase, LiveServerTestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
# coverage run --include='story6_app/*' manage.py test
# coverage report -m
class PesanTest(TestCase):
    @classmethod
    def setUpClass(cls):
        # super(TwitterLiteTest, cls).setUpClass()
        super().setUpClass()


    def test_get_index(self):
        var = Client().get('/')
        self.assertEqual(var.status_code, 200)
    
    def test_string(self):
        var = Client().get('/')
        self.assertIn('Status', var.content.decode('UTF-8'))

    def test_form(self):
        var = Client().get('/')
        self.assertIn('<form', var.content.decode())

    def test_post(self):
        var = Client().post('/tambah/', data = {'pesan' : 'Haloooo!!!!'})
        var2 = Client().get('/')
        self.assertIn('Haloooo!!!!', var2.content.decode('UTF-8'))


class PesanFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # chrome_options.setBinary('/usr/local/bin/')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.get(self.live_server_url)

    def tearDown(self):
        self.browser.quit()
        # super().tearDown()

    def test_post(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_id('pesan')
        submit = self.browser.find_element_by_id('updateButton')
        status.send_keys('Coba Coba')
        submit.send_keys("\n")
        self.browser.get(self.live_server_url)
        self.assertIn('Coba Coba', self.browser.page_source) 
        # self.tearDown()

    def test_delete(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_id('pesan')
        submit = self.browser.find_element_by_id('updateButton')
        status.send_keys('Coba Coba')
        submit.send_keys("\n")
        self.assertIn('Coba Coba', self.browser.page_source) 
        delete = self.browser.find_element_by_id('deleteButton')
        delete.send_keys("\n")
        self.assertNotIn('Coba Coba', self.browser.page_source)
        # self.tearDown()
